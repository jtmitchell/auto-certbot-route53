FROM certbot/dns-route53:v0.31.0

COPY start.sh /start.sh
COPY certbot.crontab /etc/periodic/weekly/certbot.crontab

RUN chmod +x /start.sh /etc/periodic/weekly/certbot.crontab

ENTRYPOINT /start.sh && crond -f