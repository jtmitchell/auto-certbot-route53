# auto-certbot-route53

Extends the [certbot/dns-route53](https://hub.docker.com/r/certbot/dns-route53) container to automatically register domains.
This can be used alongside the [jwilder/nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy) container to pre-generate the certificates
for SSL/TLS termination on the proxy.

## Setup

The container needs to have an email and AWS credentials so it can register the domains and manipulate Route53 on your behalf. It also expects to have a comma-separated list of domains to register.

## Running alongside nginx-proxy

* The proxy needs to run in a shared network with the containers it will proxy for. 
```
docker network create front-end
```
