#!/bin/sh

LIST=`echo $DOMAIN_LIST | tr "," "\n"`
for DOMAIN in ${LIST}
do
    if [ ! -d /etc/letsencrypt/live/{$DOMAIN} ]; then
        echo Generate $DOMAIN
        certbot certonly --dns-route53 -d ${DOMAIN} -d *.${DOMAIN} -m ${EMAIL} --agree-tos --non-interactive || cat /var/log/letsencrypt/letsencrypt.log
    fi
done

certbot renew

DOMAIN_DIRS=`ls -d /etc/letsencrypt/live/*/`
for DIR in ${DOMAIN_DIRS}
do
    DOMAIN=`basename ${DIR}`
    echo Copy ${DOMAIN} certificate
    cp /etc/letsencrypt/live/${DOMAIN}/privkey.pem /etc/nginx/certs/${DOMAIN}.key
    cp /etc/letsencrypt/live/${DOMAIN}/fullchain.pem /etc/nginx/certs/${DOMAIN}.crt
done